-- 5.9.1 Passing Continuation
-- transform program to cps version
data ScmExpr = ScmInt Int -- Atom
             | ScmVar String -- Atom
             | ScmNil -- Atom
             | ScmBool Bool -- Atom
             | ScmPrim String
             | ScmQuote ScmExpr
             | ScmIf ScmExpr ScmExpr ScmExpr
             | ScmBegin [ScmExpr]
             | ScmInvokeSet String ScmExpr
             | ScmLambda [String] [ScmExpr] -- function takes a fixed number of args.
             | ScmApp ScmExpr [ScmExpr]

type K = ScmExpr -> ScmExpr
type Kasta = [ScmExpr] -> ScmExpr
type G = K -> ScmExpr -- compare with the type of cps
cps :: ScmExpr -> K -> ScmExpr
cps e@(ScmInt _) k = k e -- atom case
cps e@(ScmVar _) k = k e -- atom case
cps e@ScmNil k = k e
cps e@(ScmBool _) k = k e
cps e@(ScmPrim _) k = k e
cps (ScmQuote e) k = cpsQuote e k
cps (ScmIf pred tc fc) k = cpsIf pred tc fc k
cps (ScmBegin es) k = cpsBegin es k
cps (ScmInvokeSet var e) k = cpsInvokeSet var e k
cps (ScmLambda vars es) k = cpsLambda vars es k
cps (ScmApp f args) k = cpsApp f args k

type ScmBody = [ScmExpr]

runCps :: ScmExpr -> ScmExpr
runCps e = cps e id

cpsQuote :: ScmExpr -> K -> ScmExpr
cpsQuote e k = k (ScmQuote e)

cpsIf :: ScmExpr -> ScmExpr -> ScmExpr -> K -> ScmExpr -- ScmExpr -> ScmExpr -> ScmExpr -> G
cpsIf pred tc fc k = let gpred = cps pred :: G
                         gtc = cps tc :: G
                         gfc = cps fc :: G
                         k' e' = ScmIf e' (gtc k) (gfc k) :: ScmExpr -- -- k' :: ScmExpr -> ScmExpr = K
                     in gpred k'

cpsIfAnother :: ScmExpr -> ScmExpr -> ScmExpr -> K -> ScmExpr
cpsIfAnother pred tc fc k = let gpred = cps pred
                                gtc = cps tc
                                gfc = cps fc
                            in gpred (\e -> gtc (\e' -> gfc (\e'' -> k (ScmIf e e' e'')))) -- cpsIf == cpsIfAnother ???
                            -- gtc (\e -> gfc (\e' -> gpred (\e'' -> k (ScmIf e'' e e')))) is this ok?

cpsBegin :: ScmBody -> K -> ScmExpr -- ScmBody -> G
cpsBegin [] k = k ScmNil
cpsBegin (e:es) k = let g = cps e :: G
                        gs = cpsBegin es :: G
                    in gs (\e' -> g (\e'' -> k (ScmApp (ScmLambda ["void"] [e']) [e''])))

cpsInvokeSet :: String -> ScmExpr -> K -> ScmExpr
cpsInvokeSet var e k = let g = cps e :: G
                           k' e' = k (ScmInvokeSet var e') :: ScmExpr -- k' :: ScmExpr -> ScmExpr = K
                       in g k'

cpsLambda :: [String] -> ScmBody -> K -> ScmExpr
cpsLambda vars es k = let gs = cpsBegin es :: G
                      in gs (\e -> k (ScmLambda ("cont":vars) [e])) -- ok???

cpsApp :: ScmExpr -> [ScmExpr] -> K -> ScmExpr
cpsApp (f@(ScmLambda _ _)) es k = let gf = cps f
                                      gargs = cpsTerms es :: ([ScmExpr]->ScmExpr) -> ScmExpr
                                  in gf (\f -> (gargs (\args -> k (ScmApp f args))))
cpsApp (pf@(ScmPrim _)) es k = let gpf = cps pf :: G
                                   gargs = cpsTerms es :: ([ScmExpr]->ScmExpr) -> ScmExpr
                               in gpf (\pf -> gargs (\args -> k (ScmApp pf args)))
cpsApp _ _ _ = undefined

-- type Kasta = [ScmExpr] -> ScmExpr
cpsTerms :: [ScmExpr] -> ([ScmExpr]->ScmExpr) -> ScmExpr
cpsTerms [] kasta = kasta []
cpsTerms (e:es) kasta = let g = cps e :: G
                            gRest = cpsTerms es :: ([ScmExpr] -> ScmExpr) -> ScmExpr
                        in g (\car -> (gRest (\cdr -> kasta (car:cdr))))
